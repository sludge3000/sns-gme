# SNS-GME

The Stormshield Network Security Group Membership Enumerator extracts the members of the specified group and provides the configuration of each member in a test file to allow for easier auditing and analysis of the rulebase.

Usage:
Copy the 'object' and 'objectgroup' files from /usr/Firewall/ConfigFiles
into the same folder as this script then run the script from a command line.

You will be prompted to enter the group name. You must enter the name with
correct capitalization.

The output test file will have the same name as the group name specified and
will be created in the same folder as the script.

NOTE: If there is an existing file with the same name, it will be overwritten.