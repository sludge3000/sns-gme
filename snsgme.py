#!/usr/bin/env python3.7
#
# Stormshield Network Security Group Membership Enumerator
# Version: 0.01 - BETA
# Created by: Luke "sludge3000" Savage
#


# Initialize global tables
templist1 = []
templist2 = []
templist3 = []
templist4 = []

grn = input('Enter group name: ')
grnf = '[' + grn + ']' + '\n'
grno = grn + '.txt'
ifile1 = 'objectgroup'
ifile2 = 'object'
inf1 = open(ifile1)
inf2 = open(ifile2)
ouf = open(grno, 'w+')
ins = 0
ine = 0


for line in inf1:
    templist1.append(line)

ins = templist1.index(grnf)
ine = templist1.index('\n', ins)

for item in templist1[ins:ine]:
    templist2.append(item)

for line in templist2[1:]:
    templist3.append(line.rstrip('\n') + '=')

for line in inf2:
    for item in templist3:
        if item in line:
            templist4.append(line)

for line in templist4:
    ouf.write(line)
ouf.close()